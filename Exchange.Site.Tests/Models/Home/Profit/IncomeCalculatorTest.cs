using System;
using System.Collections.Generic;
using Exchange.Site.DataLayer;
using NUnit.Framework;

namespace Exchange.Site.Models.Home.Profit
{
    public class IncomeCalculatorTest
    {
        private static readonly DateTime Today = new DateTime(2019, 6, 11);

        [TestCaseSource(nameof(TestCases))]
        public Decimal Calculate(CalculatorTestCase testCase)
        {
            return IncomeCalculator.Calculate(
                testCase.BaseAmount, testCase.Buying,
                testCase.Selling, testCase.BrokerFee);
        }

        public static IEnumerable<TestCaseData> TestCases()
        {
            yield return new TestCaseData(new CalculatorTestCase
                {
                    BrokerFee = 0,
                    Buying = new CurrencyRate
                    {
                        Rate = 5,
                        Date = Today
                    },
                    Selling = new CurrencyRate
                    {
                        Rate = 5,
                        Date = Today.AddDays(1)
                    }
                })
                .SetName("1 day, rate 5->5, no broker fee")
                .Returns(0m);

            yield return new TestCaseData(new CalculatorTestCase
                {
                    BrokerFee = 0,
                    BaseAmount = 10,
                    Buying = new CurrencyRate
                    {
                        Rate = 5,
                        Date = Today
                    },
                    Selling = new CurrencyRate
                    {
                        Rate = 10,
                        Date = Today.AddDays(1)
                    }
                })
                .SetName("10$ base, 1 day, rate 5->10, no broker fee")
                .Returns(-5m);

            yield return new TestCaseData(new CalculatorTestCase
                {
                    BrokerFee = 0,
                    Buying = new CurrencyRate
                    {
                        Rate = 5,
                        Date = Today
                    },
                    Selling = new CurrencyRate
                    {
                        Rate = 10,
                        Date = Today.AddDays(1)
                    }
                })
                .SetName("1 day, rate 5->10, no broker fee")
                .Returns(-0.5m);

            yield return new TestCaseData(new CalculatorTestCase
                {
                    BrokerFee = 0,
                    Buying = new CurrencyRate
                    {
                        Rate = 5,
                        Date = Today
                    },
                    Selling = new CurrencyRate
                    {
                        Rate = 10,
                        Date = Today.AddDays(5)
                    }
                })
                .SetName("5 days, rate 5->10, no broker fee")
                .Returns(-0.5m);

            yield return new TestCaseData(new CalculatorTestCase
                {
                    Buying = new CurrencyRate
                    {
                        Rate = 5,
                        Date = Today
                    },
                    Selling = new CurrencyRate
                    {
                        Rate = 10,
                        Date = Today.AddDays(5)
                    }
                })
                .SetName("5 days, rate 5->10, 10$ per day as broker fee")
                .Returns(-5.5m);

            yield return new TestCaseData(new CalculatorTestCase
                {
                    Buying = new CurrencyRate
                    {
                        Rate = 5,
                        Date = Today
                    },
                    Selling = new CurrencyRate
                    {
                        Rate = 20,
                        Date = Today.AddDays(5)
                    }
                })
                .SetName("5 days, rate 5->20")
                .Returns(-5.75m);
            
            yield return new TestCaseData(new CalculatorTestCase
                {
                    Buying = new CurrencyRate
                    {
                        Rate = 5,
                        Date = Today
                    },
                    Selling = new CurrencyRate
                    {
                        Rate = 20,
                        Date = Today.AddDays(2)
                    }
                })
                .SetName("2 days, rate 5->20")
                .Returns(-2.75m);

            yield return new TestCaseData(new CalculatorTestCase
                {
                    Buying = new CurrencyRate
                    {
                        Rate = 20,
                        Date = Today
                    },
                    Selling = new CurrencyRate
                    {
                        Rate = 5,
                        Date = Today.AddDays(5)
                    }
                })
                .SetName("5 days, rate 20->5")
                .Returns(-2m);
        }


        public class CalculatorTestCase
        {
            public Decimal BaseAmount = 1;
            public CurrencyRate Buying;
            public CurrencyRate Selling;
            public Decimal BrokerFee = 1;
        }
    }
}