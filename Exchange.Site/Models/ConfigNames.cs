namespace Exchange.Site.Models
{
    public static class ConfigNames
    {
        public const string CONNECTION_STRING = "CONNECTION_STRING";
        public const string PROFIT_CALCULATOR = "ProfitCalculator";
        public const string FIXER_IO_ACCESS_KEY = "FIXER_IO_ACCESS_KEY";
    }
}