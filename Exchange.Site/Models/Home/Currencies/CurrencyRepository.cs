using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exchange.Site.DataLayer;
using Microsoft.EntityFrameworkCore;

namespace Exchange.Site.Models.Home.Currencies
{
    public class CurrencyRepository : ICurrencyRepository
    {
        private readonly IDbContext context;

        public CurrencyRepository(IDbContext context)
        {
            this.context = context;
        }

        public async Task<IReadOnlyList<CurrencyRate>> GetHistory(
            string fromIsoCode,
            DateTime from, DateTime to,
            IReadOnlyList<string> toIsoCodes) =>
            await context
                .Set<CurrencyRate>()
                .Where(x => x.Date >= from)
                .Where(x => x.Date <= to)
                .Where(x => x.From.IsoCode == fromIsoCode)
                .Where(x => toIsoCodes.Contains(x.To.IsoCode))
                .Include(x => x.To)
                .Include(x => x.From)
                .ToArrayAsync();

        public async Task<IReadOnlyList<Currency>> GetCurrenciesByIsoCode(
            IReadOnlyList<string> isoCodes) =>
            await context
                .Set<Currency>()
                .Where(x => isoCodes.Contains(x.IsoCode))
                .ToArrayAsync();
    }
}