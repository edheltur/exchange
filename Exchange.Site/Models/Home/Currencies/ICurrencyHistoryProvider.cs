using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Exchange.Site.DataLayer;

namespace Exchange.Site.Models.Home.Currencies
{
    public interface ICurrencyHistoryProvider
    {
        Task<IReadOnlyList<CurrencyRate>> Provide(string fromIsoCode,
            IReadOnlyList<string> toIsoCodes,
            DateTime from, DateTime to);
    }
}