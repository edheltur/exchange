using System;
using System.Threading.Tasks;

namespace Exchange.Site.Models.Home.Currencies.Rates
{
    public interface IRatesModelBuilder
    {
        Task<RatesModel> Build(decimal amountOfMoney, DateTime @from, DateTime to);
    }
}