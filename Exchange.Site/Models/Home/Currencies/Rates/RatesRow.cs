using System;
using System.Collections.Generic;

namespace Exchange.Site.Models.Home.Currencies.Rates
{
    public class RatesRow
    {
        public RatesRow(DateTime date, IReadOnlyList<decimal> rates)
        {
            Date = date;
            Rates = rates;
        }

        public DateTime Date { get; }
        public IReadOnlyList<Decimal> Rates { get; }
    }
}