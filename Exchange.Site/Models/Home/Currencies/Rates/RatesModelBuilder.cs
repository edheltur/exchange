using System;
using System.Linq;
using System.Threading.Tasks;
using Exchange.Site.Models.Home.Profit;
using Microsoft.Extensions.Options;

namespace Exchange.Site.Models.Home.Currencies.Rates
{
    public class RatesModelBuilder : IRatesModelBuilder
    {
        private readonly IProfitModelCalculator profitModelCalculator;
        private readonly ICurrencyHistoryProvider historyProvider;
        private readonly ProfitCalculatorOptions options;

        public RatesModelBuilder(
            IProfitModelCalculator profitModelCalculator, 
            ICurrencyHistoryProvider historyProvider,
            IOptionsSnapshot<ProfitCalculatorOptions> options)
        {
            this.profitModelCalculator = profitModelCalculator;
            this.historyProvider = historyProvider;
            this.options = options.Value;
        }

        public async Task<RatesModel> Build(Decimal amountOfMoney, DateTime from, DateTime to)
        {
 
            var history = await historyProvider.Provide(options.FromIsoCode, options.ToIsoCodes, from, to);
            var ratesByDate = history
                .GroupBy(x => x.Date)
                .ToDictionary(x => x.Key, x => x.ToArray());

            var profit = profitModelCalculator.FindBestIncome(
                amountOfMoney, options.BrokerFee, history);

            var header = options.ToIsoCodes
                .OrderBy(x => x)
                .Select(x => $"{options.FromIsoCode}/{x}")
                .ToArray();

            var rows = DateHelper.DatesBetween(from, to)
                .Select(CreateRatesRow)
                .ToArray();

            return new RatesModel(header, rows, profit);

            RatesRow CreateRatesRow(DateTime date)
            {
                return new RatesRow(date, ratesByDate[date]
                    .OrderBy(x => x.To.IsoCode)
                    .Select(x => x.Rate)
                    .ToArray());
            }
        }
    }
}