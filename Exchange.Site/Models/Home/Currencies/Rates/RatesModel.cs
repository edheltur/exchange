using System.Collections.Generic;
using Exchange.Site.Models.Home.Profit;

namespace Exchange.Site.Models.Home.Currencies.Rates
{
    public class RatesModel
    {
        public RatesModel(IReadOnlyList<string> header, IReadOnlyList<RatesRow> rows, ProfitModel profit)
        {
            Header = header;
            Rows = rows;
            Profit = profit;
        }

        public IReadOnlyList<string> Header { get; }
        public IReadOnlyList<RatesRow> Rows { get; }

        public ProfitModel Profit { get; }

    }
}