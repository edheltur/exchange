using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Exchange.Site.DataLayer;

namespace Exchange.Site.Models.Home.Currencies.Fetchers
{
    public interface ICurrencyRateFetcher
    {
        Task<IReadOnlyList<CurrencyRate>> GetRatesByIsoCode(DateTime date,
            string fromIsoCode, IReadOnlyList<string> toIsoCodes);
    }
}