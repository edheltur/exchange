using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Exchange.Site.Models.Home.Currencies.Fetchers.FixerIo
{
    public class FixerIoResponse
    { 
        [JsonProperty("date", Required = Required.Always)] 
        public DateTime Date { get; set; }

        [JsonProperty("base", Required = Required.Always)] 
        public string Base { get; set; }

        [JsonProperty("rates", Required = Required.Always)] 
        public Dictionary<string, Decimal> RatesByCode { get; set; }
    }
}