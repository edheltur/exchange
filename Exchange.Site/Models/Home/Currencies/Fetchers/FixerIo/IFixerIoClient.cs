using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Exchange.Site.Models.Home.Currencies.Fetchers.FixerIo
{
    public interface IFixerIoClient
    {
        Task<FixerIoResponse> GetRatesByIsoCode(DateTime date,
            string @base, IReadOnlyList<string> symbols);
    }
}