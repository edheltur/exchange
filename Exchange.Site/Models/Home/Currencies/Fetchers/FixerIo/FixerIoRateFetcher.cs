using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exchange.Site.DataLayer;

namespace Exchange.Site.Models.Home.Currencies.Fetchers.FixerIo
{
    public class FixerIoRateFetcher : ICurrencyRateFetcher
    {
        private readonly ICurrencyRepository repository;
        private readonly IFixerIoClient client;

        public FixerIoRateFetcher(ICurrencyRepository repository, IFixerIoClient client)
        {
            this.repository = repository;
            this.client = client;
        }

        public async Task<IReadOnlyList<CurrencyRate>> GetRatesByIsoCode(DateTime date,
            string fromIsoCode, IReadOnlyList<string> toIsoCodes)
        {
            var response = await client.GetRatesByIsoCode(date, fromIsoCode, toIsoCodes);
            var allIsoCodes = response
                .RatesByCode.Keys
                .Concat(new[] {response.Base})
                .ToArray();

            var currencies = await repository.GetCurrenciesByIsoCode(allIsoCodes);
            var currencyByIsoCode = currencies.ToDictionary(x => x.IsoCode, x => x);

            return response.RatesByCode
                .Select(x => new CurrencyRate
                {
                    Date = response.Date,
                    From = currencyByIsoCode[response.Base],
                    To = currencyByIsoCode[x.Key],
                    Rate = x.Value
                })
                .ToArray();
        }
    }
}