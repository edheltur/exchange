using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Exchange.Site.Models.Home.Currencies.Fetchers.FixerIo
{
    public class FixerIoClient : IFixerIoClient
    {
        private const string BASE_URL = "http://data.fixer.io/api";
        private readonly IConfiguration config;
        private readonly HttpClient httpClient;


        public FixerIoClient(IConfiguration config, HttpClient httpClient)
        {
            this.config = config;
            this.httpClient = httpClient;
        }

        public async Task<FixerIoResponse> GetRatesByIsoCode(DateTime date,
            string @base, IReadOnlyList<string> symbols)
        {
            var requestUri =
                $"{BASE_URL}/{date:yyyy-MM-dd}?" +
                $"access_key={config[ConfigNames.FIXER_IO_ACCESS_KEY]}&" +
                $"symbols={string.Join(",", symbols)}&" +
                $"base={@base}";

            var httpResponse = await httpClient.GetAsync(requestUri);
            httpResponse.EnsureSuccessStatusCode();

            var rawResponse = await httpResponse.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<FixerIoResponse>(rawResponse);
        }
    }
}