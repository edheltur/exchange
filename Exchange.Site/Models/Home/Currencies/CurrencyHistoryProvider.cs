using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exchange.Site.DataLayer;
using Exchange.Site.Models.Home.Currencies.Fetchers;

namespace Exchange.Site.Models.Home.Currencies
{
    public class CurrencyHistoryProvider : ICurrencyHistoryProvider
    {
        private readonly ICurrencyRepository repository;
        private readonly IDbContext context;
        private readonly ICurrencyRateFetcher fetcher;


        public CurrencyHistoryProvider(
            ICurrencyRepository repository,
            ICurrencyRateFetcher fetcher,
            IDbContext context)
        {
            this.repository = repository;
            this.fetcher = fetcher;
            this.context = context;
        }


        public async Task<IReadOnlyList<CurrencyRate>> Provide(string fromIsoCode,
            IReadOnlyList<string> toIsoCodes,
            DateTime from, DateTime to)
        {
            var existingRates = await repository.GetHistory(fromIsoCode, from, to, toIsoCodes);

            var existingDateIsoCodePairs = existingRates
                .Select(x => new {date = x.Date.Date, isoCode = x.To.IsoCode});

            var requiredDateIsoCodePairs = DateHelper
                .DatesBetween(from, to)
                .SelectMany(date => toIsoCodes, (date, isoCode) => new {date, isoCode});

            var missingRates = (await requiredDateIsoCodePairs
                    .Except(existingDateIsoCodePairs)
                    .GroupBy(x => x.date)
                    .Select(g => new {date = g.Key, isoCodes = g.Select(x => x.isoCode).ToArray()})
                    .Select(x => fetcher.GetRatesByIsoCode(x.date, fromIsoCode, x.isoCodes))
                    .WhenAll())
                .SelectMany(x => x)
                .ToArray();

            context.Set<CurrencyRate>().AddRange(missingRates);
            context.SaveChanges();

            return existingRates
                .Concat(missingRates)
                .ToArray();
        }
    }
}