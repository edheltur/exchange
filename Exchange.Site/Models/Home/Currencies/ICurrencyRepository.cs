using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Exchange.Site.DataLayer;

namespace Exchange.Site.Models.Home.Currencies
{
    public interface ICurrencyRepository
    {
        Task<IReadOnlyList<CurrencyRate>> GetHistory(
            string fromIsoCode, 
            DateTime from, 
            DateTime to,
            IReadOnlyList<string> toIsoCodes);

        Task<IReadOnlyList<Currency>> GetCurrenciesByIsoCode(IReadOnlyList<string> isoCode);
    }
}