using System.Collections.Generic;
using System.Threading.Tasks;

namespace Exchange.Site.Models.Home
{
    public static class EnumerableExtensions
    {
        public static Task<T[]> WhenAll<T>(this IEnumerable<Task<T>> source)
        {
            return Task.WhenAll(source);
        }
    }
}