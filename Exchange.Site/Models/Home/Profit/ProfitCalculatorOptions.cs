using System;

namespace Exchange.Site.Models.Home.Profit
{
    public class ProfitCalculatorOptions
    {
        public Decimal BrokerFee { get; set; }
        public string[] ToIsoCodes { get; set; }
        public string FromIsoCode { get; set; }
    }
}