using System.Collections.Generic;
using Exchange.Site.DataLayer;

namespace Exchange.Site.Models.Home.Profit
{
    public interface IProfitModelCalculator
    {
        ProfitModel FindBestIncome(
            decimal amountOfMoney,
            decimal brokerFee, 
            IReadOnlyList<CurrencyRate> history);
    }
}