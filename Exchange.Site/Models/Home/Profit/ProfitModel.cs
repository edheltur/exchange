using System;
using Exchange.Site.DataLayer;

namespace Exchange.Site.Models.Home.Profit
{
    public class ProfitModel
    {
        public ProfitModel(DateTime buyingDate, DateTime sellingDate, Currency currency, decimal income)
        {
            BuyingDate = buyingDate;
            SellingDate = sellingDate;
            Currency = currency;
            Income = income;
        }

        public DateTime BuyingDate { get; }
        public DateTime SellingDate { get; }
        public Currency Currency { get; }
        public Decimal Income { get; }
    }
}