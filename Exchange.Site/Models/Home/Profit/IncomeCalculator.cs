using System;
using Exchange.Site.DataLayer;

namespace Exchange.Site.Models.Home.Profit
{
    public static class IncomeCalculator
    {
        public static Decimal Calculate(Decimal baseAmountBeforeSale,
            CurrencyRate buying, CurrencyRate selling, Decimal brokerFee)
        {
            var daysInPeriod = (int) (selling.Date - buying.Date).TotalDays;
            var brokerTaxTotal = brokerFee * daysInPeriod;
            var foreignAmount = baseAmountBeforeSale * buying.Rate;
            var baseAmountAfterSale = foreignAmount / selling.Rate;
            var pureIncome = baseAmountAfterSale - baseAmountBeforeSale;

            return pureIncome - brokerTaxTotal;
        }
    }
}