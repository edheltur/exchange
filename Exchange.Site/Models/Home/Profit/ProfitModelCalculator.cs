using System.Collections.Generic;
using System.Linq;
using Exchange.Site.DataLayer;

namespace Exchange.Site.Models.Home.Profit
{
    public class ProfitModelCalculator : IProfitModelCalculator
    {
        public ProfitModel FindBestIncome(
            decimal amountOfMoney,
            decimal brokerFee,
            IReadOnlyList<CurrencyRate> history)
        {
            var best = history
                .GroupBy(rates => rates.To)
                .SelectMany(rates => rates.SelectMany(
                    buying => rates.Where(selling => selling.Date > buying.Date),
                    (buying, selling) => new
                    {
                        buying, selling,
                        currency = rates.Key,
                        income = IncomeCalculator.Calculate(amountOfMoney, buying, selling, brokerFee)
                    }))
                .OrderByDescending(x => x.income)
                .First();

            return new ProfitModel(
                best.buying.Date,
                best.selling.Date,
                best.currency,
                best.income);
        }
    }
}