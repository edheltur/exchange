using System.Linq;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;

namespace Exchange.Site.Models.Home
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddUnambiguousInterfaceImplementations(
            this IServiceCollection services)
        {
            var publicInterfaces = Assembly.GetExecutingAssembly()
                .GetTypes()
                .Where(t => t.IsInterface);

            var allTypes = Assembly.GetExecutingAssembly().GetTypes();
            foreach (var publicInterface in publicInterfaces)
            {
                var implementations = allTypes
                    .Where(c => c.IsClass)
                    .Where(c => c.IsPublic)
                    .Where(c => !c.IsGenericType)
                    .Where(c => !c.IsAbstract)
                    .Where(c => publicInterface.IsAssignableFrom(c))
                    .ToArray();

                if (implementations.Length == 1)
                    services.AddScoped(publicInterface, implementations.First());
            }

            return services;
        }
    }
}