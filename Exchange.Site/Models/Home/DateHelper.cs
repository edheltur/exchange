using System;
using System.Collections.Generic;
using System.Linq;

namespace Exchange.Site.Models.Home
{
    public static class DateHelper
    {
        public static IEnumerable<DateTime> DatesBetween(DateTime from, DateTime to)
        {
            return Enumerable
                .Range(0, (int) (to.Date - from.Date).TotalDays + 1)
                .Select(i => from.AddDays(i));
        }
    }
}