using System;
using Exchange.Site.Models.Home.Validation;

namespace Exchange.Site.Models.Home
{
    public class PeriodForm
    {
        public DateTime StartDate { get; set; }
        
        [BeforeToday]
        [MaxDaysSince(nameof(StartDate), 60)]
        public DateTime EndDate { get; set; }
        public Decimal Amount { get; set; }
    }
}