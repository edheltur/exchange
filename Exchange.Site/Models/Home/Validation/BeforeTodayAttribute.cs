using System;
using System.ComponentModel.DataAnnotations;

namespace Exchange.Site.Models.Home.Validation
{
    public class BeforeTodayAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(
            object value, ValidationContext validationContext)
        {
            var date = (DateTime) value;
            if (date.Date >= DateTime.Now.Date)
            {
                return new ValidationResult(
                    "Please specify date in the past",
                    new[] {validationContext.MemberName});
            }

            return ValidationResult.Success;
        }
    }
}