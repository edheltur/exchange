using System;
using System.ComponentModel.DataAnnotations;

namespace Exchange.Site.Models.Home.Validation
{
    public class MaxDaysSinceAttribute : ValidationAttribute
    {
        private readonly string sincePropertyName;
        private readonly int maxDays;

        public MaxDaysSinceAttribute(string sincePropertyName, int maxDays)
        {
            this.sincePropertyName = sincePropertyName;
            this.maxDays = maxDays;
        }

        protected override ValidationResult IsValid(
            object value, ValidationContext validationContext)
        {
            var endDate = (DateTime) value;
            var sinceProperty = validationContext.ObjectType.GetProperty(sincePropertyName);
            var startDate = (DateTime) sinceProperty.GetValue(validationContext.ObjectInstance);
            if ((endDate - startDate).TotalDays > maxDays)
            {
                return new ValidationResult(
                    $"Specified period must not exceed {maxDays} days",
                    new[] {validationContext.MemberName});
            }

            return ValidationResult.Success;
        }
    }
}