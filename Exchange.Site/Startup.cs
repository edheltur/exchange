﻿using System;
using Exchange.Site.DataLayer;
using Exchange.Site.Models.Home;
using Exchange.Site.Models.Home.Currencies.Fetchers.FixerIo;
using Exchange.Site.Models.Home.Profit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using static Exchange.Site.Models.ConfigNames;

namespace Exchange.Site
{
    public class Startup
    {
        private readonly IConfiguration config;

        public Startup(IConfiguration config)
        {
            this.config = config;
        }


        public void ConfigureServices(IServiceCollection services)
        {
            services.AddUnambiguousInterfaceImplementations();
            services.Configure<ProfitCalculatorOptions>(config.GetSection(PROFIT_CALCULATOR));

            services.AddHttpClient<IFixerIoClient, FixerIoClient>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(1));
            
            services.AddEntityFrameworkNpgsql();
            services.AddDbContext<ExchangeContext>(options =>
                options.UseNpgsql(config[CONNECTION_STRING]));

            services
                .AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                serviceScope.ServiceProvider
                    .GetService<ExchangeContext>()
                    .Database
                    .Migrate();
            }
        }
    }
}