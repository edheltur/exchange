using System.ComponentModel.DataAnnotations;

namespace Exchange.Site.DataLayer
{
    public class Currency
    {
        public int CurrencyId { get; set; }
        
        [Required]
        [StringLength(3)]
        public string IsoCode { get; set; }
    }
}