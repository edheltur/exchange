using Microsoft.EntityFrameworkCore;

namespace Exchange.Site.DataLayer
{
    public class ExchangeContext : DbContext, IDbContext
    {
        public ExchangeContext(DbContextOptions<ExchangeContext> options)
            : base(options)
        {
        }

        public DbSet<Currency> Currencies { get; set; }

        public DbSet<CurrencyRate> CurrencyRates { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Currency>()
                .HasIndex(e => e.IsoCode);

            modelBuilder
                .Entity<Currency>()
                .HasData(
                    new Currency {CurrencyId = 1, IsoCode = "USD"},
                    new Currency {CurrencyId = 2, IsoCode = "RUB"},
                    new Currency {CurrencyId = 3, IsoCode = "EUR"},
                    new Currency {CurrencyId = 4, IsoCode = "GBP"},
                    new Currency {CurrencyId = 5, IsoCode = "JPY"}
                );
                
                
            modelBuilder
                .Entity<CurrencyRate>()
                .HasIndex(e => e.Date);
        }
    }
}