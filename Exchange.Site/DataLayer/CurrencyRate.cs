using System;
using System.ComponentModel.DataAnnotations;

namespace Exchange.Site.DataLayer
{
    public class CurrencyRate
    {
        public int CurrencyRateId { get; set; }
        
        [Required]
        public Currency From { get; set; }
        
        [Required]
        public Currency To { get; set; }
        
        public Decimal Rate { get; set; }
        
        public DateTime Date { get; set; }
        
    }
}