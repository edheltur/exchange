﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Exchange.Site.Models;
using Exchange.Site.Models.Home;
using Exchange.Site.Models.Home.Currencies.Rates;

namespace Exchange.Site.Controllers
{
    public class HomeController : Controller
    {
        
        private readonly IRatesModelBuilder ratesModelBuilder;

        public HomeController(IRatesModelBuilder ratesModelBuilder)
        {
            this.ratesModelBuilder = ratesModelBuilder;
        }

        [HttpGet("/")]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("/rates")]
        public async Task<IActionResult> Rates(decimal amount, DateTime from, DateTime to)
        {
            var model = await ratesModelBuilder.Build(amount, from, to);
            return View(model);
        }

        [HttpPost("/")]
        public IActionResult Index(PeriodForm form)
        {
            if (!ModelState.IsValid)
                return View(form);

            return RedirectToAction("Rates", new
            {
                amount = form.Amount,
                to = form.EndDate.ToString("yyyy-MM-dd"),
                from = form.StartDate.ToString("yyyy-MM-dd")
            });
        }

        [HttpGet("/privacy")]
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}